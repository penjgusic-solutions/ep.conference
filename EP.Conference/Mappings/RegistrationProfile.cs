﻿using AutoMapper;
using EP.Conference.Models.Registration;
using EP.Conference.Request;

namespace EP.Conference.Mappings
{
    public class RegistrationProfile : Profile
    {
        public RegistrationProfile()
        {
            CreateMap<RegistrationFields, RegisterRequest>().ReverseMap();
        }
    }
}