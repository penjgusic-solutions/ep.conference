﻿using AutoMapper;
using EP.Conference.Models.Speakers;
using EP.Conference.Models.Speakers.Details;

namespace EP.Conference.Mappings
{
    public class SpeakersProfile : Profile
    {
        public SpeakersProfile()
        {
            CreateMap<ExtendedSpeakersField, SpeakerFields>().ReverseMap();

            CreateMap<SpeakerDetailsViewModel, SpeakerViewModel>();

            CreateMap<SpeakersDetailsFields, SpeakerFields>().ReverseMap();
        }
    }
}
