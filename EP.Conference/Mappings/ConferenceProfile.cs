﻿using AutoMapper;
using EP.Conference.Models.Conference;
using EP.Conference.Request.Conference;

namespace EP.Conference.Mappings
{
    public class ConferenceProfile : Profile
    {
        public ConferenceProfile()
        {
            CreateMap<ConferenceFields, ConferenceRequest>();
        }
    }
}
