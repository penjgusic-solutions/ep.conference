﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using EP.Conference.Contracts;
using EP.Conference.Models.Conference;
using EP.Conference.Models.Registration;
using EP.Conference.Models.Speakers;
using EP.Conference.Models.Speakers.Details;
using EP.Conference.Request;
using EP.Conference.Request.Conference;
using Newtonsoft.Json;
using RestSharp;

namespace EP.Conference.Services
{
    public class GenesisService : IGenesisService
    {
        private readonly IGenesisFactory _factory;
        private readonly IMapper _mapper;

        public GenesisService(IGenesisFactory factory, IMapper mapper)
        {
            _factory = factory;
            _mapper = mapper;
        }

        public async Task DeleteConferenceAsync(string id)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/EVENT/{id}", Method.DELETE, DataFormat.Json);

            await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }

        public async Task UpdateConferenceAsync(ConferenceFields model)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/EVENT/{model.GGUID}", Method.PUT, DataFormat.Json);

            request.AddHeader("If-Match", model.INSERTUSER);

            var body = _mapper.Map<ConferenceFields, ConferenceRequest>(model);

            var data = new { fields = body };

            var serialized = JsonConvert.SerializeObject(data);

            request.AddJsonBody(serialized);

            await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }

        public async Task CreateConferenceAsync(ConferenceFields model)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/EVENT", Method.POST, DataFormat.Json);

            var body = _mapper.Map<ConferenceFields, ConferenceRequest>(model);

            var data = new {fields = body};

            var serialized = JsonConvert.SerializeObject(data);

            request.AddJsonBody(serialized);

            await client.ExecuteAsync(request, cancellationTokenSource.Token);

        }

        public async Task<List<ConferenceViewModel>> GetConferencesAsync()
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest("/v3.0/type/EVENT/list", Method.GET, DataFormat.Json);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);

            var response = JsonConvert.DeserializeObject<List<ConferenceViewModel>>(restResponse.Content);

            return response;
        }

        public async Task<ConferenceViewModel> GetConferenceAsync(string id)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/EVENT/{id}", Method.GET, DataFormat.Json);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);

            var response = JsonConvert.DeserializeObject<ConferenceViewModel>(restResponse.Content);

            response.Fields.INSERTUSER = restResponse.Headers.FirstOrDefault(x => x.Name.ToUpperInvariant() == "ETAG")?.Value.ToString();

            return response;
        }

        public async Task DeleteRegistrationAsync(string id)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/REGISTRATION/{id}", Method.DELETE, DataFormat.Json);

            await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }

        public async Task CreateRegistrationAsync(RegistrationFields registerFields)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/REGISTRATION", Method.POST, DataFormat.Json);

            var body = _mapper.Map<RegistrationFields, RegisterRequest>(registerFields);

            var data = new { fields = body };

            var serialized = JsonConvert.SerializeObject(data);

            request.AddJsonBody(serialized);

            await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }

        public async Task<List<RegistrationViewModel>> GetRegistrationsAsync()
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest("/v3.0/type/REGISTRATION/list", Method.GET, DataFormat.Json);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);

            var response = JsonConvert.DeserializeObject<List<RegistrationViewModel>>(restResponse.Content);

            return response;
        }

        public async Task<RegistrationViewModel> GetRegistrationAsync(string id)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/REGISTRATION/{id}", Method.GET, DataFormat.Json);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);

            var response = JsonConvert.DeserializeObject<RegistrationViewModel>(restResponse.Content);

            return response;
        }

        public  async Task<List<SpeakerViewModel>> GetSpeakersAsync()
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest("/v3.0/type/ADDRESS/full", Method.GET, DataFormat.Json);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);

            var response = JsonConvert.DeserializeObject<List<SpeakerDetailsViewModel>>(restResponse.Content);

            response = response.Where(x => x.Fields.GWBRANCH == "Education and Teaching").ToList();

            var result = new List<SpeakerViewModel>();

            _mapper.Map(response, result);

            return result;
        }

        public async Task<ExtendedSpeakersField> GetSpeakerAsync(string id)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/ADDRESS/{id}", Method.GET, DataFormat.Json);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);

            var response = JsonConvert.DeserializeObject<SpeakerViewModel>(restResponse.Content);

            var result = _mapper.Map<SpeakerFields, ExtendedSpeakersField>(response.Fields);

           result.Etag = restResponse.Headers.FirstOrDefault(x => x.Name.ToUpperInvariant() == "ETAG")?.Value.ToString();

            return result;
        }

        public async Task UpdateSpeakerAsync(ExtendedSpeakersField model)
        {
            model.GWBRANCH = "Education and Teaching";

            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/ADDRESS/{model.GGUID}", Method.PUT, DataFormat.Json);

            request.AddHeader("If-Match", model.Etag);

            var body = _mapper.Map<ExtendedSpeakersField, SpeakerFields>(model);

            var data = new { fields = body };

            var serialized = JsonConvert.SerializeObject(data);

            request.AddJsonBody(serialized);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }

        public async Task CreateSpeakerAsync(SpeakerFields model)
        {
            model.GWBRANCH = "Education and Teaching";

            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/ADDRESS", Method.POST, DataFormat.Json);

            var data = new { fields = model };

            var serialized = JsonConvert.SerializeObject(data);

            request.AddJsonBody(serialized);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }

        public async Task DeleteSpeakerAsync(string id)
        {
            var client = _factory.GetGenesisClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/ADDRESS/{id}", Method.DELETE, DataFormat.Json);

            await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }
    }
}