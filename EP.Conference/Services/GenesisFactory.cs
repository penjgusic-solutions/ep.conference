﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;
using EP.Conference.Contracts;
using EP.Conference.Settings;
using Microsoft.Extensions.Options;
using RestSharp;

namespace EP.Conference.Services
{
    public class GenesisFactory : IGenesisFactory
    {
        private readonly HttpSettings _settings;

        public GenesisFactory(IOptions<HttpSettings> settings)
        {
            _settings = settings.Value;
        }

        public RestClient GetGenesisClient()
        {
            var encoding = new UTF8Encoding();

            var client = new RestClient(_settings.Url) {CachePolicy = new RequestCachePolicy(RequestCacheLevel.Default)};


            string authInfo = Convert.ToBase64String(encoding.GetBytes(_settings.Username + ":" + _settings.Password));

            client.AddDefaultHeader("Authorization", $"Basic {authInfo}");
            client.AddDefaultHeader("X-CAS-PRODUCT-KEY", _settings.ProductKey);
            client.Timeout = 15000;

            return client;
        }
    }
}
