﻿using System;
using System.ComponentModel.DataAnnotations;

// ReSharper disable InconsistentNaming
// ReSharper disable IdentifierTypo
// ReSharper disable StringLiteralTypo

namespace EP.Conference.Models.Conference
{
    public class ConferenceFields
    {

        [Key] public string GGUID { get; set; }

        public bool DISABLEWAITLIST { get; set; }

        public bool EV_ALLOWADDRCHANGE { get; set; }

        public bool EV_ALLOWPUBLICREGISTRATION { get; set; }

        public bool EV_ALLOWREGTOAPP { get; set; }

        public bool EV_AUTOCONFIRMREG { get; set; }

        [Display(Name = "Kraj")]
        public DateTime EV_ENDDT { get; set; }

        public bool EV_FURTHERPARTICIPANTS { get; set; }

        [Display(Name = "Početak")]
        public DateTime EV_STARTDT { get; set; }

        [Display(Name = "Naziv konferencije")]
        public string EV_TITLE { get; set; }

        [Display(Name = "Lokacija")]
        public string EV_VENUE { get; set; }

        public int FOREIGNEDITPERMISSION { get; set; }

        public bool GWRTFINFOFLAG { get; set; }

        public DateTime INSERTTIMESTAMP { get; set; }

        public string INSERTUSER { get; set; }

        public string OWNERGUID { get; set; }

        public string OWNERNAME { get; set; }

        public bool RESOURCERESERVATION { get; set; }

        public DateTime UPDATETIMESTAMP { get; set; }

        public string UPDATEUSER { get; set; }
    }
}