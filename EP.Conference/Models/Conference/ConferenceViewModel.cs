﻿using EP.Conference.Models.Shared;

namespace EP.Conference.Models.Conference
{
    public class ConferenceViewModel
    {
        public string ObjectType { get; set; }

        public string Id { get; set; }

        public ConferenceFields Fields { get; set; }

        public Links Links { get; set; }
    }
}