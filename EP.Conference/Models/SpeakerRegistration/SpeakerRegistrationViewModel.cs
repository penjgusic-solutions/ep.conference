﻿using EP.Conference.Models.Conference;
using EP.Conference.Models.Speakers;

namespace EP.Conference.Models.SpeakerRegistration
{
    public class SpeakerRegistrationViewModel
    {
        public SpeakerFields Speaker { get; set; }

        public ConferenceFields Conferences { get; set; }
    }
}
