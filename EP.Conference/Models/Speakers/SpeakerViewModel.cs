﻿using EP.Conference.Models.Shared;

namespace EP.Conference.Models.Speakers
{
    public class SpeakerViewModel
    {
        public string ObjectType { get; set; }

        public string Id { get; set; }

        public SpeakerFields Fields { get; set; }

        public Links Links { get; set; }
    }
}
