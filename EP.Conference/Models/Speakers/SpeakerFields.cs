﻿// ReSharper disable InconsistentNaming
// ReSharper disable IdentifierTypo
// ReSharper disable StringLiteralTypo

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EP.Conference.Models.Speakers
{
    public class SpeakerFields
    {
        [DisplayName("Ime")]
        public string CHRISTIANNAME { get; set; }

        [DisplayName("Prezime")]
        public string NAME { get; set; }

        [DisplayName("Id")]
        [Key] public string GGUID { get; set; }

        [DisplayName("Kompanija")]
        public string COMPNAME { get; set; }

        public string GWBRANCH { get; set; }
    }
}