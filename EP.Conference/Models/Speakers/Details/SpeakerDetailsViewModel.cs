﻿using EP.Conference.Models.Shared;

namespace EP.Conference.Models.Speakers.Details
{
    public class SpeakerDetailsViewModel
    {
        public string ObjectType { get; set; }

        public string Id { get; set; }

        public SpeakersDetailsFields Fields { get; set; }

        public Links Links { get; set; }
    }
}