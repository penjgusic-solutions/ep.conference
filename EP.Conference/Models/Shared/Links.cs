﻿namespace EP.Conference.Models.Shared
{
    public class Links
    {
        public string Self { get; set; }

        public string Dossier { get; set; }
    }
}
