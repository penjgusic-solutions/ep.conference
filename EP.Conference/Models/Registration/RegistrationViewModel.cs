﻿using EP.Conference.Models.Shared;

namespace EP.Conference.Models.Registration
{
    public class RegistrationViewModel
    {
        public string ObjectType { get; set; }

        public string Id { get; set; }

        public RegistrationFields Fields { get; set; }

        public Links Links { get; set; }
    }
}