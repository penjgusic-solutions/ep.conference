﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

// ReSharper disable InconsistentNaming
// ReSharper disable IdentifierTypo
// ReSharper disable StringLiteralTypo

namespace EP.Conference.Models.Registration
{
    public class RegistrationFields
    {
        public int FOREIGNEDITPERMISSION { get; set; }

        [Key]
        public string GGUID { get; set; }

        public bool GWRTFINFOFLAG { get; set; }

        public DateTime INSERTTIMESTAMP { get; set; }

        public string INSERTUSER { get; set; }

        public string OWNERGUID { get; set; }

        public string OWNERNAME { get; set; }

        public string REG_ADDGUID { get; set; }

        [DisplayName("Predavač")]
        public string REG_ADDSHORTINFO { get; set; }

        [DisplayName("Kraj")]
        public DateTime REG_END_DT { get; set; }

        public string REG_EVENTGUID { get; set; }

        public bool REG_ISPUBLIC { get; set; }

        [DisplayName("Konferencija")]
        public string REG_KEYWORD { get; set; }

        public string REG_PAYSHORTINFO { get; set; }

        public DateTime REG_REGISTRATIONDATE { get; set; }

        public string REG_REGISTRATIONSTATE { get; set; }

        public string REG_REGSHORTINFO { get; set; }

        [DisplayName("Početak")]
        public DateTime REG_START_DT { get; set; }

        public string REG_TYPE { get; set; }

        public DateTime UPDATETIMESTAMP { get; set; }

        public string UPDATEUSER { get; set; }
    }
}
