﻿
// ReSharper disable InconsistentNaming
// ReSharper disable IdentifierTypo
// ReSharper disable StringLiteralTypo

using System;
using EP.Conference.Converters;
using Newtonsoft.Json;

namespace EP.Conference.Request.Conference
{
    public class ConferenceRequest
    {
        [JsonConverter(typeof(GenesisDateConverter))]
        public DateTime EV_STARTDT { get; set; }

        [JsonConverter(typeof(GenesisDateConverter))]
        public DateTime EV_ENDDT { get; set; }

        public string EV_TITLE { get; set; }

        public string EV_VENUE { get; set; }
    }
}
