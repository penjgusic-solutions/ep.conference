﻿namespace EP.Conference.Request
{
    public class RegisterRequest
    {
        public string REG_EVENTGUID { get; set; }

        public string REG_ADDGUID { get; set; }

        public string REG_REGISTRATIONSTATE { get; set; }

        public string REG_ADDSHORTINFO { get; set; }

        public string REG_REGSHORTINFO { get; set; }

        public string REG_KEYWORD { get; set; }
    }
}
