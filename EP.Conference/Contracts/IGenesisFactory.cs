﻿using RestSharp;

namespace EP.Conference.Contracts
{
    public interface IGenesisFactory
    {
        RestClient GetGenesisClient();
    }
}
