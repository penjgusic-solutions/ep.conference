﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EP.Conference.Models.Conference;
using EP.Conference.Models.Registration;
using EP.Conference.Models.Speakers;

namespace EP.Conference.Contracts
{
    public interface IGenesisService
    {
        #region Registration

        Task DeleteRegistrationAsync(string id);

        Task CreateRegistrationAsync(RegistrationFields registerFields);

        Task<List<RegistrationViewModel>> GetRegistrationsAsync();

        Task<RegistrationViewModel> GetRegistrationAsync(string id);

        #endregion

        #region Speakers

        Task<List<SpeakerViewModel>> GetSpeakersAsync();

        Task<ExtendedSpeakersField> GetSpeakerAsync(string id);

        Task UpdateSpeakerAsync(ExtendedSpeakersField model);

        Task CreateSpeakerAsync(SpeakerFields model);

        Task DeleteSpeakerAsync(string id);

        #endregion

        #region Conference

        Task DeleteConferenceAsync(string id);

        Task UpdateConferenceAsync(ConferenceFields model);

        Task CreateConferenceAsync(ConferenceFields model);

        Task<List<ConferenceViewModel>> GetConferencesAsync();

        Task<ConferenceViewModel> GetConferenceAsync(string id);

        #endregion
    }
}