﻿using EP.Conference.Models.Conference;
using EP.Conference.Models.Registration;
using EP.Conference.Models.Speakers;
using Microsoft.EntityFrameworkCore;

namespace EP.Conference.Context
{
    public class ConferenceDbContext : DbContext
    {
        public ConferenceDbContext(DbContextOptions<ConferenceDbContext> options) : base(options)
        {
        }

        public DbSet<ConferenceFields> Conferences { get; set; }

        public DbSet<SpeakerFields> Speakers { get; set; }

        public DbSet<RegistrationFields> Registrations { get; set; }
    }
}