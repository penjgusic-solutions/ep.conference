﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;

namespace EP.Conference.Converters
{
    public class GenesisDateConverter : IsoDateTimeConverter
    {
        public GenesisDateConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.fff'Z'";
        }
    }
}
