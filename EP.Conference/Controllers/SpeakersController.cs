﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EP.Conference.Contracts;
using EP.Conference.Models.Conference;
using EP.Conference.Models.Registration;
using EP.Conference.Models.Speakers;
using Microsoft.AspNetCore.Mvc;

namespace EP.Conference.Controllers
{
    public class SpeakersController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IGenesisService _service;

        public SpeakersController(IGenesisService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: Speakers
        public async Task<IActionResult> Index()
        {
            var response = await _service.GetSpeakersAsync();

            return View(response.Select(x => x.Fields));
        }

        // GET: Speakers/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetSpeakerAsync(id);

            if (response == null) return NotFound();

            return View(response);
        }

        // GET: Speakers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Speakers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CHRISTIANNAME,NAME,GGUID,COMPNAME")]SpeakerFields speakerFields)
        {
            if (ModelState.IsValid)
            {
                await _service.CreateSpeakerAsync(speakerFields);
                return RedirectToAction(nameof(Index));
            }

            return View(speakerFields);
        }

        public async Task<IActionResult> Register(string id, string firstName, string lastName)
        {
            var response = await _service.GetConferencesAsync();

            var fields = response.Select(x => x.Fields).ToList();
            fields.ForEach(x =>
            {
                x.UPDATEUSER = id;
                x.INSERTUSER = firstName;
                x.OWNERNAME = lastName;
            });

            return View(fields);
        }

        public async Task<IActionResult> RegisterMe(string eventId, string speakerId, string firstName, string lastName, string conferenceName)
        {
            if (ModelState.IsValid)
            {
                var registrationFields = new RegistrationFields
                {
                    REG_EVENTGUID = $"0x{eventId}",
                    REG_ADDGUID = $"0x{speakerId}",
                    REG_ADDSHORTINFO = $"{firstName} {lastName}",
                    REG_REGSHORTINFO = $"{firstName} {lastName}",
                    REG_KEYWORD = conferenceName,
                    REG_REGISTRATIONSTATE = "confirmed"
                };
                await _service.CreateRegistrationAsync(registrationFields);

                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Index));
        }

        // GET: Speakers/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetSpeakerAsync(id);

            if (response == null) return NotFound();

            TempData["Etag"] = response.Etag;

            return View(response);
        }

        // POST: Speakers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CHRISTIANNAME,NAME,GGUID,COMPNAME")]
            SpeakerFields speakerFields)
        {
            if (id != speakerFields.GGUID) return NotFound();

            if (ModelState.IsValid)
            {
                var model = _mapper.Map<SpeakerFields, ExtendedSpeakersField>(speakerFields);
                model.Etag = TempData["Etag"].ToString();

                await _service.UpdateSpeakerAsync(model);

                return RedirectToAction(nameof(Index));
            }

            return View(speakerFields);
        }

        // GET: Speakers/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetSpeakerAsync(id);

            if (response == null) return NotFound();

            return View(response);
        }

        // POST: Speakers/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _service.DeleteSpeakerAsync(id);

            return RedirectToAction(nameof(Index));
        }
      
    }
}