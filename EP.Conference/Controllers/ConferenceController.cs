﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EP.Conference.Contracts;
using EP.Conference.Models.Conference;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EP.Conference.Controllers
{
    public class ConferenceController : Controller
    {
        private readonly IGenesisService _service;

        public ConferenceController(IGenesisService service)
        {
            _service = service;
        }

        // GET: ConferenceFields
        public async Task<IActionResult> Index()
        {
            var response = await _service.GetConferencesAsync();

            return View(response.Select(x => x.Fields));
        }

        // GET: ConferenceFields/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetConferenceAsync(id);

            if (response == null) return NotFound();

            return View(response.Fields);
        }

        // GET: ConferenceFields/Create
        public IActionResult Create()
        {
            var model = new ConferenceFields {EV_STARTDT = DateTime.Now, EV_ENDDT = DateTime.Now.AddDays(1)};

            return View(model);
        }

        // POST: ConferenceFields/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EV_TITLE,EV_STARTDT,EV_ENDDT,EV_VENUE")]ConferenceFields conferenceFields)
        {
            await _service.CreateConferenceAsync(conferenceFields);
            if (ModelState.IsValid) return RedirectToAction(nameof(Index));
            return View(conferenceFields);
        }

        // GET: ConferenceFields/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetConferenceAsync(id);

            if (response == null) return NotFound();

            TempData["Etag"] = response.Fields.INSERTUSER;

            return View(response.Fields);
        }

        // POST: ConferenceFields/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("GGUID,EV_TITLE,EV_STARTDT,EV_ENDDT,EV_VENUE,INSERTUSER")]
            ConferenceFields conferenceFields)
        {
            if (id != conferenceFields.GGUID) return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    conferenceFields.INSERTUSER = TempData["Etag"].ToString();
                    await _service.UpdateConferenceAsync(conferenceFields);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConferenceFieldsExists(conferenceFields.GGUID))
                        return NotFound();
                    throw;
                }

                return RedirectToAction(nameof(Index));
            }

            return View(conferenceFields);
        }

        // GET: ConferenceFields/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetConferenceAsync(id);

            if (response == null) return NotFound();

            return View(response.Fields);
        }

        // POST: ConferenceFields/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _service.DeleteConferenceAsync(id);

            return RedirectToAction(nameof(Index));
        }

        private bool ConferenceFieldsExists(string id)
        {
            //return _context.Conferences.Any(e => e.GGUID == id);
            return true;
        }
    }
}