﻿using System.Linq;
using System.Threading.Tasks;
using EP.Conference.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace EP.Conference.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IGenesisService _service;

        public RegistrationController(IGenesisService service)
        {
            _service = service;
        }

        // GET: Registration
        public async Task<IActionResult> Index(string id)
        {
            var response = await _service.GetRegistrationsAsync();
            var fields = response.Select(x => x.Fields);
            var filtered = fields.Where(x => x.REG_EVENTGUID.Contains(id)).ToList();

            return View(filtered);
        }

        // GET: Registration/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetRegistrationAsync(id);

            if (response == null) return NotFound();

            return View(response.Fields);
        }

        // GET: Registration/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetRegistrationAsync(id);

            if (response == null) return NotFound();

            return View(response.Fields);
        }

        // POST: Registration/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _service.DeleteRegistrationAsync(id);

            return RedirectToAction(nameof(Index), nameof(Conference));
        }
    }
}